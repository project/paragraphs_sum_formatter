<?php

namespace Drupal\paragraphs_sum_formatter\Plugin\Field\FieldFormatter;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\entity_reference_revisions\Plugin\Field\FieldFormatter\EntityReferenceRevisionsFormatterBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'Paragraphs sum' formatter.
 *
 * @FieldFormatter(
 *   id = "paragraphs_sum_formatter",
 *   label = @Translation("Paragraphs sum"),
 *   field_types = {
 *     "entity_reference_revisions"
 *   }
 * )
 */
final class SumFormatter extends EntityReferenceRevisionsFormatterBase implements ContainerFactoryPluginInterface {

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $bundleInfo;

  /**
   * Constructs a ParagraphsSumFormatter object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Third party settings.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $bundle_info
   *   The bundle info service.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, EntityFieldManagerInterface $entity_field_manager, EntityTypeBundleInfoInterface $bundle_info) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->entityFieldManager = $entity_field_manager;
    $this->bundleInfo = $bundle_info;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('entity_field.manager'),
      $container->get('entity_type.bundle.info')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings(): array {
    $setting = [
      'field' => '',
      'label' => 'Total:',
      'show_count' => FALSE,
      'count_label' => 'Count:',
      'show_count_first' => FALSE,
    ];

    return $setting + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {

    $fields = $this->getBundleFields();

    $element['field'] = [
      '#type' => 'select',
      '#title' => $this->t('Field to sum'),
      '#default_value' => $this->getSetting('field'),
      '#options' => $fields,
    ];

    $element['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#default_value' => $this->getSetting('label'),
    ];

    // Checkbox to show also the count.
    $element['show_count'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show count'),
      '#default_value' => $this->getSetting('show_count'),
    ];

    $element['count_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Count label'),
      '#default_value' => $this->getSetting('count_label'),
      '#states' => [
        'visible' => [
          ':input[name*="[settings][show_count]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $element['show_count_first'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show count first'),
      '#default_value' => $this->getSetting('show_count_first'),
      '#states' => [
        'visible' => [
          ':input[name*="[settings][show_count]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(): array {
    $summary = [
      $this->t('Field: @field', ['@field' => empty($this->getSetting('field')) ? 'No field selected' : $this->getSetting('field')]),
      $this->t('Label: @field', ['@field' => empty($this->getSetting('label')) ? 'No Label' : $this->getSetting('label')]),
      $this->t('Show count: @field', ['@field' => empty($this->getSetting('show_count')) ? 'No' : 'Yes']),
    ];

    if ($this->getSetting('show_count')) {
      $summary[] = $this->t('Count label: @field', ['@field' => empty($this->getSetting('count_label')) ? 'No Label' : $this->getSetting('count_label')]);
      $summary[] = $this->t('Show count first: @field', ['@field' => empty($this->getSetting('show_count_first')) ? 'No' : 'Yes']);
    }
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {

    $element = [];
    $total = 0;
    $count = 0;

    $sum_field = $this->getSetting('field');
    if (empty($sum_field)) {
      return $element;
    }

    foreach ($this->getEntitiesToView($items, $langcode) as $delta => $entity) {

      if ($entity instanceof FieldableEntityInterface && $entity->id()) {

        if (!$entity->hasField($sum_field)) {
          continue;
        }

        $count++;
        $field = $entity->get($sum_field);
        $field_articles_value = $field->value;
        // Use intval() to sum integer fields.
        if ($field->getFieldDefinition()->getType() == 'integer' || $field->getFieldDefinition()->getType() == 'list_integer') {
          $total += intval($field_articles_value);
        }
        // Use floatval() to sum float fields.
        elseif ($field->getFieldDefinition()->getType() == 'decimal' || $field->getFieldDefinition()->getType() == 'float' || $field->getFieldDefinition()->getType() == 'list_float') {
          $total += floatval($field_articles_value);
        }

      }
    }

    $element[0] = [
      '#theme' => 'paragraphs_sum_formatter',
      '#label' => $this->getSetting('label'),
      '#total' => $total,
      '#count' => $this->getSetting('show_count') ? $count : NULL,
      '#count_label' => $this->getSetting('count_label'),
      '#show_count_first' => $this->getSetting('show_count_first'),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    $storage = $field_definition->getFieldStorageDefinition();
    return $storage->isMultiple() && $storage->getSetting('target_type') === 'paragraph';
  }

  /**
   * Get all bundles that can be assigned to this entity reference.
   *
   * @return array
   *   An array of bundle keys.
   */
  public function getAssignableBundles() {

    $bundles = [];

    $field_settings = $this->fieldDefinition->getSettings();
    if (!empty($field_settings['handler_settings']['target_bundles'])) {
      $bundles = $field_settings['handler_settings']['target_bundles'];
    }
    else {
      foreach ($this->bundleInfo->getBundleInfo('paragraph') as $bundle_name => $bundle) {
        $bundles[$bundle_name] = $bundle['label'];
      }
    }

    return $bundles;
  }

  /**
   * Gets a bundle fields array suitable for form options.
   *
   * @return array
   *   The fields array that can be passed to form element of type select.
   */
  protected function getBundleFields() {

    foreach ($this->getAssignableBundles() as $bundle_name => $bundle) {
      $field_definitions = $this->entityFieldManager->getFieldDefinitions("paragraph", $bundle_name);

      foreach ($field_definitions as $field_name => $field_definition) {
        if (!$field_definition->getFieldStorageDefinition()->isBaseField()) {
          $field_type = $field_definition->getType();
          // Only add numeric fields.
          $numeric_types = ['integer', 'decimal', 'float', 'list_integer', 'list_float'];
          if (in_array($field_type, $numeric_types)) {
            $bundle_fields[$field_name] = $field_definition->getLabel();
          }

        }
      }
    }

    return $bundle_fields ?? [];
  }

}

## INTRODUCTION

Paragraphs Sum formatter module provides a formatter that displays the sum of a configurable paragraph field.

## REQUIREMENTS

[Paragraphs](https://www.drupal.org/project/paragraphs)

## INSTALLATION

Install as you would normally install a contributed Drupal module.
See: https://www.drupal.org/node/895232 for further information.

## CONFIGURATION
1. Create a new paragraph type.
2. Add a numeric field to the paragraph type.
3. Create a new Entity reference revisions Paragraph field on the content type.
4. Go to the display settings of the content type.
5. Select the Paragraphs Sum formatter for the paragraph field.
6. Configure the formatter to use the numeric field you want to sum.
